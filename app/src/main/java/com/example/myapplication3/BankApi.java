package com.example.myapplication3;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BankApi {
    @GET("api/atm")
    Call<List<Atm>> getData(@Query("city") String city);
}

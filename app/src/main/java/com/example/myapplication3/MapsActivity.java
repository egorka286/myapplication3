package com.example.myapplication3;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.example.myapplication3.databinding.ActivityMapsBinding;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String baseUrl = "https://belarusbank.by/";
    private static final String city = "Гомель";
    private static final String TAG = "myapplication3";
    private GoogleMap mMap;
    private static BankApi bankApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.example.myapplication3.databinding.ActivityMapsBinding binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        bankApi = retrofit.create(BankApi.class);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        bankApi.getData(city).enqueue(new Callback<List<Atm>>() {
            @Override
            public void onResponse(@NonNull Call<List<Atm>> call, @NonNull Response<List<Atm>> response) {
                if (response.body() != null) {
                    for (Atm atm : response.body()) {
                        LatLng xy = new LatLng(Double.parseDouble(atm.getGpsX()), Double.parseDouble(atm.getGpsY()));
                        mMap.addMarker(new MarkerOptions().position(xy).title(atm.getAddressType() + atm.getAddress() + " " + atm.getHouse()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Atm>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }
}
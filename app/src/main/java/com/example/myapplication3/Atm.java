package com.example.myapplication3;

//import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class Atm {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city_type")
    @Expose
    private String cityType;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address_type")
    @Expose
    private String addressType;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("house")
    @Expose
    private String house;
    @SerializedName("install_place")
    @Expose
    private String installPlace;
    @SerializedName("work_time")
    @Expose
    private String workTime;
    @SerializedName("gps_x")
    @Expose
    private String gpsX;
    @SerializedName("gps_y")
    @Expose
    private String gpsY;
    @SerializedName("install_place_full")
    @Expose
    private String installPlaceFull;
    @SerializedName("work_time_full")
    @Expose
    private String workTimeFull;
    @SerializedName("ATM_type")
    @Expose
    private String aTMType;
    @SerializedName("ATM_error")
    @Expose
    private String aTMError;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("cash_in")
    @Expose
    private String cashIn;
    @SerializedName("ATM_printer")
    @Expose
    private String aTMPrinter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public void setInstallPlace(String installPlace) {
        this.installPlace = installPlace;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getGpsX() {
        return gpsX;
    }

    public void setGpsX(String gpsX) {
        this.gpsX = gpsX;
    }

    public String getGpsY() {
        return gpsY;
    }

    public void setGpsY(String gpsY) {
        this.gpsY = gpsY;
    }

    public String getInstallPlaceFull() {
        return installPlaceFull;
    }

    public void setInstallPlaceFull(String installPlaceFull) {
        this.installPlaceFull = installPlaceFull;
    }

    public String getWorkTimeFull() {
        return workTimeFull;
    }

    public void setWorkTimeFull(String workTimeFull) {
        this.workTimeFull = workTimeFull;
    }

    public String getATMType() {
        return aTMType;
    }

    public void setATMType(String aTMType) {
        this.aTMType = aTMType;
    }

    public String getATMError() {
        return aTMError;
    }

    public void setATMError(String aTMError) {
        this.aTMError = aTMError;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCashIn() {
        return cashIn;
    }

    public void setCashIn(String cashIn) {
        this.cashIn = cashIn;
    }

    public String getATMPrinter() {
        return aTMPrinter;
    }

    public void setATMPrinter(String aTMPrinter) {
        this.aTMPrinter = aTMPrinter;
    }

}